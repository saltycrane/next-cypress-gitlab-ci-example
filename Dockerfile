# https://github.com/cypress-io/cypress-docker-images/blob/master/base/14.16.0/Dockerfile
FROM cypress/base:14.16.0

WORKDIR /app
# run npm install before adding app code for better Docker caching
# https://semaphoreci.com/docs/docker/docker-layer-caching.html
COPY ./package.json /app
COPY ./package-lock.json /app
# CI=true suppresses Cypress progress log spam
RUN CI=true npm ci
COPY . /app
RUN npm run build
